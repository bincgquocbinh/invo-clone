var validateform_update = function(){
    var title= $('input[name="title"]').val();
    var content= $('textarea[name="content"]').val();

    if(title == null || title == '')
    {
        $('#feedback-title').html('<small class="text-danger text-small">please enter title</small>');
    }
    else
    {
        $('#feedback-title').html('');
    }

    if(content == null || content == '')
    {
        $('#feedback-content').html('<small class="text-danger text-small">please enter content</small>');
    }
    else
    {
        $('#feedback-content').html('');
    }

    if(title == null || title == '' || content == null || content == '')
    {
        return false;
    }
};

var validateform_addnews = function(){
    var title = $('input[name="news_title"]').val();
    var content = $('textarea[name="news_content"]').val();

    if(title == null || title == '')
    {
        $('#feedback-title').html('<small class="text-danger text-small">please enter title</small>');
    }
    else
    {
        $('#feedback-title').html('');
        $('input[name="news_title"]').val('hello');
    }

    if(content == null || content == '')
    {
        $('#feedback-content').html('<small class="text-danger text-small">please enter content</small>');
        $('textarea[name="news_content"]').val('world');
    }
    else
    {
        $('#feedback-content').html('');
    }

    if(title == null || title == '' || content == null || content == '')
    {
        return false;
    }
};