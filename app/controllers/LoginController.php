<?php
use Phalcon\Tag as Tag;
use Phalcon\Http\Response;
class LoginController extends ControllerBase
{

	public function initialize()
    {
        Tag::setTitle('Sign Up/Sign In');
        parent::initialize();

    }

    public function indexAction()
    {
            $this->response->redirect('/login');
    }

    public function showAction()
    {
    	if (!$this->request->isPost()) {
    		// fill the input ('name', 'value')
            Tag::setDefault('user_email', 'quocbinh@yopmail.com');
            Tag::setDefault('user_password', '1');
        }
    }

    public function validateUserAction()
    {
        $res = new Response();
    	if ($this->request->isPost())
    	{

            $email = $this->request->getPost('user_email', 'email');
            $password = sha1($this->request->getPost('user_password'));
            if($email == '' or $password == '')
            {
                $this->flashSession->error('Email or Password can not be null');
                return $this->response->redirect('/login');
            }

            $user = Users::findFirst("email='$email' AND password='$password' AND active='Y'");

            if ($user != false)
            {
                $this->init_session($user);
//                print_r($this->session->get('human_Auth')); die;
                $this->flashSession->success('Welcome ' . $user->name . ' to board ... Good morning ');
                return $res->redirect('/news/list'); // pass router redirect
            }

            $this->flashSession->error('Wrong email/password');
        }
        return $res->redirect('/login'); // pass router redirect
    }

    private function init_session($u)
    {
    	$this->session->set('human_Auth', [
            'id' => $u->id,
            'name' => $u->name,
            'username' => $u->username
        ]);
    }

    public function registerAccountAction()
    {
    	$req = $this->request;
    	if ($req->isPost()) 
    	{
//    	    print_r($req->getPost()); die;
            if ($req->getPost('user_password') !=  $req->getPost('user_password_repeat'))
            {
                $this->flash->error('Passwords are diferent');
                return false;
            }

    	    $user = new Users();
    	    $user->name = $req->getPost('fullname');
    	    $user->username = $req->getPost('username');
    	    $user->email = $req->getPost('user_email');
    	    $user->password = sha1($req->getPost('user_password'));
    	    $user->created_at = new Phalcon\Db\RawValue('now()');
    	    $user->active = 'Y';
            if ($user->save() == false)
            {
                foreach ($user->getMessages() as $message)
                {
                    $this->flash->error((string) $message.' [err]');
                }
            }
            else
            {
                Tag::setDefault('user_email', $user->email);
                Tag::setDefault('user_password', $req->getPost('user_password'));
                $this->flash->success('Thanks for sign-up, please log-in to start generating invoices');
                return $this->forward('login/index');
            }
    	}

    }

    public function goodbyeAction()
    {
        $this->session->remove('human_Auth');
        $this->flash->success('Goodbye!');
        return $this->response->redirect('/login');;
    }

    public function notFoundAction()
    {
    }
}

