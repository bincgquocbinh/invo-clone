<?php
use Phalcon\Http\Response;
class ControllerBase extends Phalcon\Mvc\Controller
{

    protected function initialize()
    {
        Phalcon\Tag::prependTitle('INVO | ');
        if($this->session->has("human_Auth"))
        {
            $this->view->menu = ['Home' => '/welcome', 'News' => '/news/list', 'Logout' => '/logout'];
        }
    }

    protected function forward($uri){
        $uriParts = explode('/', $uri);
        return $this->dispatcher->forward(
            array(
                'controller' => $uriParts[0],
                'action' => $uriParts[1]
            )
        );
    }
}
