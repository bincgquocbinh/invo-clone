<?php

use Phalcon\Mvc\Controller;

class TestingController extends Controller
{

    public function indexAction()
    {
    	echo "<h1>hello</h1>";
    }

    public function sumAction($a, $b)
    {
    	echo "<h2> function sumAction</h2>";
    	echo "result sum is: ". ($a+$b);
    }

    public function getDateAction($d, $m, $y)
    {
    	echo "<h2> function getDateAction</h2>";
    	echo "result date is: ". $d .'/'. $m . '/'. $y;
    }

}

