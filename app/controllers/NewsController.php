<?php
use Phalcon\Tag;
class NewsController extends ControllerBase
{

    public function initialize()
    {
        Tag::setTitle('News - Listing');
        parent::initialize();

    }

    public function indexAction()
    {
    }

    public function showAction($data_prepare=null)
    {
        $news_model = new News();

        $news_listing = $news_model->find(["order" => "id DESC"]);
        $this->view->setVar("news_listing", $news_listing);

        // form add new section
        if (!$this->request->isPost() and $data_prepare == null) {
            $this->view->setVar("news_title", 'What is Lorem Ipsum?');
            $this->view->setVar('news_content', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters');
        }
        else
        {
            $this->view->setVar("news_title", $data_prepare['title']);
            $this->view->setVar('news_content', $data_prepare['content']);
        }
    }

    public function addAction()
    {
        if($this->request->isPost())
        {
            if($this->request->getPost('news_title') == '' or $this->request->getPost('news_content') == '')
            {
                $this->flashSession->error('title or content can not be null in [add function]');
                return $this->response->redirect('/news/list');
            }

            $news_model = new News();
            $news_checker = News::findFirst('title="' . $this->request->getPost('news_title') . '"');
            if(count($news_checker->title) > 0)
            {
                $this->flashSession->error('This title existed before, please think about other title [add function]');
                Tag::setDefault('news_title', $this->request->getPost('news_title'));
                Tag::setDefault('news_content', $this->request->getPost('news_content'));
                return $this->dispatcher->forward(['controller' => 'news', 'action' => 'show']);
            }
            $news_model->date_create = new Phalcon\Db\RawValue('now()');
            $news_model->title = $this->request->getPost('news_title');
            $news_model->content = $this->request->getPost('news_content');
            $news_model->status = "Y";
            $news_model->username = $this->session->human_Auth['username'];
            if ($news_model->save() == false)
            {
                foreach ($news_model->getMessages() as $message)
                {
                    $this->flash->error((string) $message.' [err]');
                }
            }
            else
            {
                $this->flashSession->success("create success the news, thanks for your contribution");
                return $this->response->redirect(
                    [
                        'action' => 'show',
                        'params'
                    ]
                );
            }
        }
        else
        {
            return $this->showAction();
        }
    }

    public function updateAction($id)
    {
        $request = $this->request;
//        display value news to form edit
        if (!$request->isPost()) {

            $id = $this->filter->sanitize($id, array("int"));

            $news_listing = News::findFirst('id="' . $id . '"');
            if (!$news_listing)
            {
                $this->flashSession->error("This news was not found - news 's ID = " . $id);
                return $this->forward("news/show");
            }

            $this->view->setVar("id", $news_listing->id);

            Tag::displayTo("id", $news_listing->id);
            Tag::displayTo("title", $news_listing->title);
            Tag::displayTo("content", $news_listing->content);
            Tag::displayTo("status", $news_listing->status);
        }
    }

    public function saveAction()
    {
        $request = $this->request;
        if (!$request->isPost())
        {
            return $this->forward("news/list");
        }
        $id = $request->getPost("id", "int");
        if($request->getPost("title") == "" or $request->getPost("content") == "")
        {
            $this->flashSession->error("title or content can not be null");
            return $this->response->redirect('/news/update/' . $id);
        }
        $news_checker = News::findFirst('title="' . $request->getPost("title") . '"');
        if(count($news_checker->title) > 0)
        {
            $this->flashSession->error("This title existed before, please think about other title [update function]");
            return $this->response->redirect('/news/update/' . $id);
        }
        $news_listing = News::findFirst("id='$id'");

        if ($news_listing == false)
        {
            $this->flashSession->error("The news does not exist with id = ".$id);
            return $this->response->redirect("news/list");
        }

        $news_listing->id = $request->getPost("id", "int");
        $news_listing->title = $request->getPost("title");
        $news_listing->content = $request->getPost("content");
        $news_listing->status = $request->getPost("status");

        if (!$news_listing->save())
        {
            foreach ($news_listing->getMessages() as $message)
            {
                $this->flashSession->error((string) $message);
            }
            return $this->response->redirect("news/update/" . $news_listing->id);
        }
        else
        {
            $this->flashSession->success("News was successfully updated");
            return $this->response->redirect("news/list");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $news = News::findFirst('id="' . $id . '"');
        if (!$news)
        {
            $this->flashSession->error("This news was not found - news 's ID = " . $id);
            return $this->response->redirect("news/list");
        }

        if (!$news->delete())
        {
            foreach ($news->getMessages() as $message)
            {
                $this->Session->error((string) $message);
            }
        }
        $this->flashSession->success('the news is deleted');
        return $this->response->redirect("news/list");
    }


}

