<?php

//namespace Offshore\Backend\Controllers;
use Offshore\Repositories\Country;
//use Offshore\Models\OccLanguage;
use Offshore\Utils\Validator;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Offshore\Repositories\Activity;
class LanguageController extends ControllerBase
{
	public function indexAction()
	{
        $current_page = $this->request->get('page');
//        $validator = new Validator();
//        if($validator->validInt($current_page) == false || $current_page < 1)
//            $current_page=1;
        $keyword = trim($this->request->get("txtSearch"));
        $sql = "SELECT * FROM OccLanguage WHERE 1";
        $arrParameter = array();
        // check keyword not empty and pass - to view
        if(!empty($keyword)){
//            if($validator->validInt($keyword)) {
//                $sql.=" AND (language_id = :keyword:) ";
//            } else {
                $sql.=" AND (language_name like CONCAT('%',:keyword:,'%'))";
//            }
            $arrParameter['keyword'] = $keyword;
            $this->dispatcher->setParam("txtSearch", $keyword);
        }
        $sql.=" ORDER BY language_name ASC"; // done SQL string

        $list_language = $this->modelsManager->executeQuery($sql,$arrParameter);
        $paginator = new PaginatorModel(array(
                'data'  => $list_language,
                'limit' => 20,
                'page'  => $current_page,
            ));
        if ($this->session->has('msg_result')) {
            $msg_result = $this->session->get('msg_result');
            $this->session->remove('msg_result');
            $this->view->msg_result = $msg_result;
        }
        if ($this->session->has('msg_del')) {
            $msg_result = $this->session->get('msg_del');
            $this->session->remove('msg_del');
            $this->view->msg_del = $msg_result;
        }
        $this->view->list_language = $paginator->getPaginate();
	}


    public function createAction()
    {
        $data = array('language_id' => -1,'language_active' => 'Y', 'language_order' => 1, );
        if($this->request->isPost()) {
            $messages = array();
            $data = array(
                'language_name' => $this->request->getPost("txtName", array('string', 'trim')),
                'language_code' => $this->request->getPost("txtCode", array('string', 'trim')),
                'language_country_code' => $this->request->getPost("slcCountry", array('string', 'trim')),
                'language_order' => $this->request->getPost("txtOrder", array('string', 'trim')),
                'language_active' => $this->request->getPost("radActive"),
            );
            if (empty($data["language_name"])) {
                $messages["name"] = "Name field is required.";
            } 
            if($data['language_code'] == "") {
                $messages['code'] = 'Code field is required.';
            }else if(Language::checkCode($data['language_code'],-1)) {
                    $messages["code"] = "Code is exists.";
            }      
            if (empty($data['language_order'])) {
                $messages["order"] = "Order field is required.";
            } else if (!is_numeric($data["language_order"])) {
                $messages["order"] = "Order is not valid ";
            }
            if (count($messages) == 0) {
                $msg_result = array();
                $new_language = new OccLanguage();
                $new_language->setLanguageName($data["language_name"]);
                $new_language->setLanguageCode($data["language_code"]);
                $new_language->setLanguageCountryCode($data["language_country_code"]);
                $new_language->setLanguageOrder($data["language_order"]);
                $new_language->setLanguageActive($data["language_active"]);
                $result = $new_language->save();
                $data_log = json_encode(array());
                if ($result === true) {
                    $msg_result = array('status' => 'success', 'msg' => 'Create Language Success');
                    $old_data = array();
                    $new_data = $data;
                    $data_log = json_encode(array('occ_language' => array($new_language->getLanguageId() => array($old_data, $new_data))));

                } else {
                    $message = "We can't store your info now: \n";
                    foreach ($new_language->getMessages() as $msg) {
                        $message .= $msg . "\n";
                    }
                    $msg_result['status'] = 'error';
                    $msg_result['msg'] = $message;
                }
               // $activity = new Activity();
              //  $activity->logActivity($this->controllerName, $this->actionName, $this->auth['id'], $message,
              //      $data_log);
                $this->session->set('msg_result', $msg_result);
                return $this->response->redirect("/dashboard/list-language");
            }
        }
        $select_country = "";// Country::getComboboxByCode($data['language_country_code']);
        $messages["status"] = "border-red";
        $this->view->setVars([
            'formData' => $data,
            'messages' => $messages,
            'select_country' => $select_country,
        ]);
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function editAction()
    {
        $id = $this->request->get('id');
     //   $checkID = new Validator();
      //  if(!$checkID->validInt($id))
     //   {
   //         return $this->response->redirect('notfound');
   //     }
        $language_model = OccLanguage::findFirstById($id);
        if(empty($language_model))
        {
            return $this->response->redirect('notfound');
        }
        $model_data = array(
            'language_id' => $language_model->getLanguageId(),
            'language_name' => $language_model->getLanguageName(),
            'language_code' => $language_model->getLanguageCode(),
            'language_country_code' => $language_model->getLanguageCountryCode(),
            'language_order' => $language_model->getLanguageOrder(),
            'language_active' => $language_model->getLanguageActive(),
        );
        $input_data = $model_data;
        $messages = array();
        if($this->request->isPost()) {
            $data = array(
                'language_id' => $id,
                'language_name' => $this->request->getPost("txtName", array('string', 'trim')),
                'language_code' => $this->request->getPost("txtCode", array('string', 'trim')),
                'language_country_code' => $this->request->getPost("slcCountry", array('string', 'trim')),
                'language_order' => $this->request->getPost("txtOrder", array('string', 'trim')),
                'language_active' => $this->request->getPost("radActive"),
            );
            $input_data = $data;
            if (empty($data["language_name"])) {
                $messages["name"] = "Name field is required.";
            }
            if($data['language_code'] == "") {
                $messages['code'] = 'Code field is required.';
            }else if(Language::checkCode($data['language_code'],$data['language_id'])) {
                    $messages["code"] = "Code is exists.";
            }
            if (empty($data['language_order'])) {
                $messages["order"] = "Order field is required.";
            } else if (!is_numeric($data["language_order"])) {
                $messages["order"] = "Order is not valid ";
            }
            if (count($messages) == 0) {
                $msg_result = array();
                $language_model->setLanguageName($data["language_name"]);
                $language_model->setLanguageCode($data["language_code"]);
                $language_model->setLanguageCountryCode($data["language_country_code"]);
                $language_model->setLanguageOrder($data["language_order"]);
                $language_model->setLanguageActive($data["language_active"]);
                $result = $language_model->update();
                $data_log = json_encode(array());
                if ($result === true) {
                    $old_data = $model_data;
                    $new_data = $input_data;
                    $data_log = json_encode(array('occ_langauge' => array($id => array($old_data, $new_data))));
                    $msg_result = array('status' => 'success', 'msg' => 'Edit Area Success');
                } else {
                    $message = "We can't store your info now: \n";
                    foreach ($language_model->getMessages() as $msg) {
                        $message .= $msg . "\n";
                    }
                    $msg_result['status'] = 'error';
                    $msg_result['msg'] = $message;
                }
              //  $activity = new Activity();
             //   $activity->logActivity($this->controllerName, $this->actionName, $this->auth['id'], $message,
              //      $data_log);
                $this->session->set('msg_result', $msg_result);
                return $this->response->redirect("/dashboard/list-language");
            }
        }
        $select_country ='';// Country::getComboboxByCode($input_data['language_country_code']);
        $messages["status"] = "border-red";
        $this->view->setVars([
            'formData' => $input_data,
            'messages' => $messages,
            'select_country' => $select_country,
        ]);
    }
    public function deleteAction()
    {
        $language_checked = $this->request->getPost("item");
        if (!empty($language_checked)) {
            $occ_log = array();
            foreach ($language_checked as $id) {
                $language_item = OccLanguage::findFirstById($id);
                if ($language_item) {
                    $msg_result = array();
                    if ($language_item->delete() === false) {
                        $message_delete = 'Can\'t delete the Language Name = '.$language_item->getLanguageName();
                        $msg_result['status'] = 'error';
                        $msg_result['msg'] = $message_delete;
                    } else {
                        $old_data = array(
                            'language_id' => $language_item->getLanguageId(),
                            'language_name' => $language_item->getLanguageName(),
                            'language_code' => $language_item->getLanguageCode(),
                            'language_country_code' => $language_item->getLanguageCountryCode(),
                            'language_active' => $language_item->getLanguageActive(),
                            'language_order' => $language_item->getLanguageOrder(),
                        );
                        $occ_log[$id] = $old_data;
                    }
                }
            }
            if(count($occ_log) > 0) {
                $message_delete = 'Delete '. count($occ_log) .' Language successfully.';
                $msg_result['status'] = 'success';
                $msg_result['msg'] = $message_delete;
                $message = '';
                $data_log = json_encode(array('occ_language' => $occ_log));
                $activity = new Activity();
                $activity->logActivity($this->controllerName, $this->actionName, $this->auth['id'], $message, $data_log);
            }
            $this->session->set('msg_result', $msg_result);
            return $this->response->redirect("/dashboard/list-language");
        }
    }
    public function toolAction()
    {
        $table = $this->request->get('slcTable');
        $language_code = $this->request->get('slcLanguage');
        $strLanguage = Language::getComboTool($language_code,$this->globalVariable->defaultLanguage);
        $strTable = Language::getComboTable($table);
        $result = array();
        if($this->request->isPost()) {
            if(empty($table)){
                $msg_table = 'Table is required.';
            }
            if(empty($language_code)) {
                $msg_lang = 'Language is required.';
            }
        }
        if(!empty($table)&&(!empty($language_code))){
            $result = $this->getValueByTable($table,$language_code);
        }
        $this->view->setVars([
            'strLanguage' => $strLanguage,
            'strTable' => $strTable,
            'result' => $result,
            'msg_table' => $msg_table,
            'msg_lang' => $msg_lang
        ]);
    }

    public function toolexportAction()
    {
        $table = $this->request->get('slcTable');
        $language_code = $this->request->get('slcLanguage');
        $strLanguage = Language::getComboTool($language_code,$this->globalVariable->defaultLanguage);
        $strTable = Language::getComboTable($table);
        $result = array();
       // $msg_table = '';
       // $msg_lang = '';
        if($this->request->isPost()) {
            if(empty($table)){
                $msg_table = 'Table is required.';
            }
            if(empty($language_code)) {
                $msg_lang = 'Language is required.';
            }
        }
        if(!empty($table)&&(!empty($language_code))){
            $result = $this->getValueByTable($table,$language_code);
        }
        $this->view->setVars([
            'strLanguage' => $strLanguage,
            'strTable' => $strTable,
            'result' => $result,
            'msg_table' => $msg_table,
            'msg_lang' => $msg_lang
        ]);
    }

    private function getValueByTable($table,$language_code){
        $sql = "";
        switch ($table){
            case "area":
                $sql = "SELECT a.area_id as id,a.area_name as name
                            FROM \Offshore\Models\OccArea a 
                            WHERE a.area_active = 'Y' AND a.area_id NOT IN (
                              SELECT al.area_id 
                              FROM \Offshore\Models\OccAreaLang al
                              WHERE al.area_lang_code =:LANG:
                            )";
                break;
            case "country":
                $sql = "SELECT c.country_id as id, c.country_name as name, c.country_title as title, c.country_meta_keyword as meta_keyword, c.country_meta_description as meta_description
                            FROM \Offshore\Models\OccCountry c 
                            WHERE c.country_active = 'Y' AND c.country_id NOT IN (
                              SELECT cl.country_id 
                              FROM \Offshore\Models\OccCountryLang cl
                              WHERE cl.country_lang_code =:LANG:
                            )";
                break;
            case "office":
                $sql = "SELECT of.office_id as id, of.office_name as name, of.office_content as content
                            FROM \Offshore\Models\OccOffice of 
                            WHERE of.office_active = 'Y' AND of.office_id NOT IN (
                              SELECT ofl.office_id 
                              FROM \Offshore\Models\OccOfficeLang ofl
                              WHERE ofl.office_lang_code =:LANG:
                            )";
                break;
            case "contentservicetype":
                $sql = "SELECT cst.type_id as id, cst.type_name as name, cst.type_title as title, cst.type_meta_keyword as meta_keyword, cst.type_meta_description as meta_description 
                            FROM \Offshore\Models\OccContentServiceType cst 
                            WHERE cst.type_active = 'Y' AND cst.type_id NOT IN (
                              SELECT cstl.type_id 
                              FROM \Offshore\Models\OccContentServiceTypeLang cstl
                              WHERE cstl.type_lang_code =:LANG:
                            )";
                break;
            case "contentinsighttype":
                $sql = "SELECT cit.type_id as id , cit.type_name as name, cit.type_title as title, cit.type_meta_keyword as meta_keyword, cit.type_meta_description as meta_description
                            FROM \Offshore\Models\OccContentInsightType cit 
                            WHERE cit.type_active = 'Y' AND cit.type_id NOT IN (
                              SELECT citl.type_id 
                              FROM \Offshore\Models\OccContentInsightTypeLang citl
                              WHERE citl.type_lang_code =:LANG:
                            )";
                break;
            case "contentformdownloadtype":
                $sql = "SELECT cdt.type_id as id, cdt.type_name as name, cdt.type_title as title, cdt.type_meta_keyword as meta_keyword, cdt.type_meta_description as meta_description
                            FROM \Offshore\Models\OccContentFormDownloadType cdt 
                            WHERE cdt.type_active = 'Y' AND cdt.type_id NOT IN (
                              SELECT cdtl.type_id 
                              FROM \Offshore\Models\OccContentFormDownloadTypeLang cdtl
                              WHERE cdtl.type_lang_code =:LANG:
                            )";
                break;
            case "banner":
                $sql = "SELECT b.banner_id as id, b.banner_title as title, b.banner_subtitle as subtitle 
                            FROM \Offshore\Models\OccBanner b 
                            WHERE b.banner_active = 'Y' AND b.banner_id NOT IN (
                              SELECT bl.banner_id 
                              FROM \Offshore\Models\OccBannerLang bl
                              WHERE bl.banner_lang_code =:LANG:
                            )";
                break;
            case "contentservice":
                $sql = "SELECT cs.service_id as id, cs.service_name as name, cs.service_title as title, cs.service_meta_keyword as meta_keyword, cs.service_meta_description as meta_description, cs.service_summary as summary, cs.service_content as content 
                            FROM \Offshore\Models\OccContentService cs 
                            WHERE cs.service_active = 'Y' AND cs.service_id NOT IN (
                              SELECT csl.service_id 
                              FROM \Offshore\Models\OccContentServiceLang csl
                              WHERE csl.service_lang_code =:LANG:
                            )";
                break;
            case "contentpromotion":
                $sql = "SELECT cp.promotion_id as id, cp.promotion_name as name, cp.promotion_title as title, cp.promotion_meta_keyword as meta_keyword, cp.promotion_meta_description as meta_description, cp.promotion_summary as summary, cp.promotion_content as content
                            FROM \Offshore\Models\OccContentPromotion cp 
                            WHERE cp.promotion_active = 'Y' AND cp.promotion_id NOT IN (
                              SELECT cpl.promotion_id 
                              FROM \Offshore\Models\OccContentPromotionLang cpl
                              WHERE cpl.promotion_lang_code =:LANG:
                            )";
                break;
            case "contentfaq":
                $sql = "SELECT cf.faq_id as id, cf.faq_name as name, cf.faq_title as title, cf.faq_meta_keyword as meta_keyword, cf.faq_meta_description as meta_description, cf.faq_content as content
                            FROM \Offshore\Models\OccContentFaq cf 
                            WHERE cf.faq_active = 'Y' AND cf.faq_id NOT IN (
                              SELECT cfl.faq_id 
                              FROM \Offshore\Models\OccContentFaqLang cfl
                              WHERE cfl.faq_lang_code =:LANG:
                            )";
                break;
            case "contentformdownload":
                $sql = "SELECT cfd.download_id as id, cfd.download_name as name
                            FROM \Offshore\Models\OccContentFormDownload cfd 
                            WHERE cfd.download_active = 'Y' AND cfd.download_id NOT IN (
                              SELECT cfdl.download_id 
                              FROM \Offshore\Models\OccContentFormDownloadLang cfdl
                              WHERE cfdl.download_lang_code =:LANG:
                            )";
                break;
            case "contentinsight":
                $sql = "SELECT ci.insight_id as id , ci.insight_name as name, ci.insight_title as title, ci.insight_meta_keyword as meta_keyword, ci.insight_meta_description as meta_description, ci.insight_summary as summary, ci.insight_content as content
                            FROM \Offshore\Models\OccContentInsight ci 
                            WHERE ci.insight_active = 'Y' AND ci.insight_id NOT IN (
                              SELECT cil.insight_id 
                              FROM \Offshore\Models\OccContentInsightLang cil
                              WHERE cil.insight_lang_code =:LANG:
                            )";
                break;
            case "contentarticle":
                $sql = "SELECT ca.article_id as id, ca.article_name as name, ca.article_title as title, ca.article_meta_keyword as meta_keyword, ca.article_meta_description as meta_description, ca.article_summary as summary, ca.article_content as content
                            FROM \Offshore\Models\OccContentArticle ca 
                            WHERE ca.article_active = 'Y' AND ca.article_id NOT IN (
                              SELECT cal.article_id 
                              FROM \Offshore\Models\OccContentArticleLang cal
                              WHERE cal.article_lang_code =:LANG:
                            )";
                break;
            case "jurisdiction":
                $sql = "SELECT jur.jurisdiction_id as id , jur.jurisdiction_name as name, jur.jurisdiction_subname as subname, jur.jurisdiction_title as title, jur.jurisdiction_meta_description as meta_description, jur.jurisdiction_meta_keyword as meta_keyword, jur.jurisdiction_summary as summary, jur.jurisdiction_content as content
                            FROM \Offshore\Models\OccJurisdiction jur 
                            WHERE jur.jurisdiction_active = 'Y' AND jur.jurisdiction_id NOT IN (
                              SELECT jurl.jurisdiction_id 
                              FROM \Offshore\Models\OccJurisdictionLang jurl
                              WHERE jurl.jurisdiction_lang_code =:LANG:
                            )";
                break;
            case "companykit":
                $sql = "SELECT cpk.company_id as id, cpk.company_name as name, cpk.company_title as title, cpk.company_meta_description as meta_description, cpk.company_meta_keyword as meta_keyword, cpk.company_summary as summary, cpk.company_content as content
                            FROM \Offshore\Models\OccCompanyKit cpk 
                            WHERE cpk.company_active = 'Y' AND cpk.company_id NOT IN (
                              SELECT cpkl.company_id 
                              FROM \Offshore\Models\OccCompanyKitLang cpkl
                              WHERE cpkl.company_lang_code =:LANG:
                            )";
                break;
            case "scopeofservice":
                $sql = "SELECT sos.company_id as id, sos.company_name as name, sos.company_title as title, sos.company_meta_keyword as meta_keyword, sos.company_meta_description as meta_description, sos.company_summary as summary, sos.company_content as content 
                            FROM \Offshore\Models\OccScopeOfService sos 
                            WHERE sos.company_active = 'Y' AND sos.company_id NOT IN (
                              SELECT sosl.company_id 
                              FROM \Offshore\Models\OccScopeOfServiceLang sosl
                              WHERE sosl.company_lang_code =:LANG:
                            )";
                break;
            case "shelfcompany":
                $sql = "SELECT sc.she_id as id , sc.she_name as name
                            FROM \Offshore\Models\OccShelfCompany sc 
                            WHERE sc.she_active = 'Y' AND sc.she_id NOT IN (
                              SELECT scl.she_id 
                              FROM \Offshore\Models\OccShelfCompanyLang scl
                              WHERE scl.she_lang_code =:LANG:
                            )";
                break;
            case "orderservice":
                $sql = "SELECT os.service_id as id, os.service_name as name, os.service_description as description, os.service_suffix_description as suffix_description
                            FROM \Offshore\Models\OccOrderService os 
                            WHERE os.service_active = 'Y' AND os.service_id NOT IN (
                              SELECT osl.service_id 
                              FROM \Offshore\Models\OccOrderServiceLang osl
                              WHERE osl.service_lang_code =:LANG:
                            )";
                break;
            case "config":
                $sql = "SELECT c.config_key as id, c.config_content as name
                            FROM \Offshore\Models\OccConfig c 
                            WHERE c.config_language = 'en' AND c.config_key NOT IN (
                              SELECT cl.config_key 
                              FROM \Offshore\Models\OccConfig cl
                              WHERE cl.config_language =:LANG:
                            )";
                break;
            case "page":
                $sql = "SELECT p.page_id as id, p.page_name as name, p.page_title as title, p.page_meta_keyword as meta_keyword, p.page_meta_description as meta_description, p.page_content as content 
                            FROM \Offshore\Models\OccPage p 
                            WHERE p.page_id NOT IN (
                              SELECT pl.page_id 
                              FROM \Offshore\Models\OccPageLang pl
                              WHERE pl.page_lang_code =:LANG:
                            )";
                break;
            case "suffix":
                $sql = "SELECT s.suffix_id as id, s.suffix_name as name 
                            FROM \Offshore\Models\OccSuffix s 
                            WHERE s.suffix_active = 'Y' AND s.suffix_id NOT IN (
                              SELECT sl.suffix_id 
                              FROM \Offshore\Models\OccSuffixLang sl
                              WHERE sl.suffix_lang_code =:LANG:
                            )";
                break;
            case "credittype":
                $sql = "SELECT t.type_id as id, t.type_name as name
                            FROM \Offshore\Models\OccCreditType t 
                            WHERE t.type_active = 'Y' AND type_id NOT IN (
                              SELECT tl.type_id 
                              FROM \Offshore\Models\OccCreditTypeLang tl
                              WHERE tl.type_lang_code =:LANG:
                            )";
                break;
            case "templateemail":
                $sql = "SELECT t.email_id as id 
                            FROM \Offshore\Models\OccTemplateEmail t
                            WHERE email_id NOT IN (
                              SELECT tl.email_id 
                              FROM \Offshore\Models\OccTemplateEmailLang tl
                              WHERE tl.email_lang_code =:LANG:
                            )";
                break;
        }
        $result = array();
        if(!empty($sql)){
            $result = $this->modelsManager->executeQuery($sql,array('LANG'=>$language_code));
        }
        return $result;
    }
}
