<?php

//namespace Offshore\Repositories;

use Phalcon\Mvc\User\Component;
use Offshore\Models\OccAreaLang;

class AreaLang extends Component
{
        public static  function deleteById($id){
            $arr_lang = OccAreaLang::findById($id);
            foreach ($arr_lang as $lang){
                $lang->delete();
            }
        }
        public static  function findFirstByIdAndLang($id,$lang_code){
            return OccAreaLang::findFirst(array (
                "area_id = :ID: AND area_lang_code = :CODE:",
                'bind' => array('ID' => $id,
                                'CODE' => $lang_code )));
        }
}



