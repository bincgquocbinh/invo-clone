<?php

//namespace Offshore\Repositories;

//use Offshore\Models\OccLanguage;
use Phalcon\Mvc\User\Component;

class Language extends Component {
    public static function checkCode($language_code, $language_id)
    {
        return OccLanguage::findFirst(
            array (
                'language_code = :CODE: AND language_id != :languageid:',
                'bind' => array('CODE' => $language_code, 'languageid' => $language_id),
            ));
    }
    /**
     * @return OccLanguage|OccLanguage[]
     */
    public static function getLanguages(){
        return OccLanguage::find(array("language_active = 'Y'",
            "order" => "language_order"));
    }
    public static function  findAllLanguage(){
        $ar_lang = array();
        $list_language = OccLanguage::find('language_active ="Y"');
        if (sizeof($list_language)>0){
            foreach ($list_language as $item){
                $ar_lang[] = $item->getLanguageCode();
            }
        }
        return $ar_lang;
    }
    public static function getCombo($lang_code){
        $list_language = self::getLanguages();
        $string = "";
        foreach ($list_language as $language){
            $selected = '';
            if($language->getLanguageCode() == $lang_code)
            {
                $selected = 'selected';
            }
            $string .= "<option ".$selected." value='".$language->getLanguageCode()."'>".$language->getLanguageName()."</option>";
        }
        return $string;
    }
    public static function getComboTool($lang_code,$defaultLanguage){
        $list_language = self::getLanguages();
        $string = "";
        foreach ($list_language as $language){
            $selected = '';
            if($language->getLanguageCode() == $lang_code)
            {
                $selected = 'selected';
            }
            if($language->getLanguageCode() != $defaultLanguage) {
                $string .= "<option " . $selected . " value='" . $language->getLanguageCode() . "'>" . $language->getLanguageName() . "</option>";
            }
        }
        return $string;
    }
    public static function getComboTable($code){
        $list_table = array(
            'area' =>'Area',
            'country' => 'Country',
            'office' => 'Office',
            'contentservicetype' => 'Service Type',
            'contentinsighttype' => 'Insight Type',
            'contentformdownloadtype' => 'Form Download Type',
            'banner' => 'Banner',
            'contentservice' => 'Content Service',
            'contentpromotion' => 'Promotion',
            'contentfaq' => 'FAQ',
            'contentformdownload' => 'Form Download',
            'contentinsight' => 'Insight',
            'contentarticle' => 'Article',
            'jurisdiction' => 'Landing page',
            'companykit' => 'Company Kit',
            'scopeofservice' => 'Scope Of Service',
            'shelfcompany' => 'Shelf Company',
            'orderservice' => 'Service',
            'config' => 'Config',
            'page' => 'Page',
            'suffix' => 'Suffix',
            'credittype' => 'Credit Type',
            'templateemail' => 'Template Email',
        );
        $string = "";
        foreach ($list_table as $key => $value){
            $selected = '';
            if($key == $code)
            {
                $selected = 'selected';
            }
            $string .= "<option " . $selected . " value='" . $key . "'>" . $value . "</option>";

        }
        return $string;
    }
    public static function arrLanguages () {
        $arr_language = array();
        $languages = self::getLanguages();
        foreach ($languages as $lang){
            $arr_language[$lang->getLanguageCode()] = $lang->getLanguageName();
        }
        return $arr_language;
    }
    public static function findAllLanguageCodes()
    {
        $ar_lang = array();
        $list_language = OccLanguage::find('language_active ="Y"');
        if (sizeof($list_language) > 0) {
            foreach ($list_language as $item) {
                $ar_lang[] = $item->getLanguageCode();
            }
        }
        return $ar_lang;
    }
    public function getActiveLanguagesByLocation($countryCode) {
        $sql = 'SELECT la.* FROM \Offshore\Models\OccLanguage la 
                INNER JOIN \Offshore\Models\OccLocation lo ON lo.location_lang_code = la.language_code
                WHERE lo.location_active="Y" AND la.language_active="Y" AND lo.location_country_code = "'.$countryCode.'"
                ORDER BY lo.location_order ASC';

        return $this->modelsManager->executeQuery($sql);
    }
    public static function getLanguageByCode($language_code)
    {
        return OccLanguage::findFirst(array('language_code = :CODE: AND language_active="Y"', 'bind' => array('CODE' => $language_code),));
    }

    public static function getNameByCode($language_code)
    {
        $occ_language = OccLanguage::findFirst(array('language_code = :CODE: AND language_active="Y"', 'bind' => array('CODE' => $language_code),));
        return $occ_language ? $occ_language->getLanguageName() : '';
    }
}
