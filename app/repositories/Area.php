<?php

//namespace Offshore\Repositories;

use Offshore\Models\OccArea;
use Phalcon\Mvc\User\Component;

class Area extends Component {
    public function checkName($area_name, $area_id)
    {
        return OccArea::findFirst(array(
            'area_name=:name: AND area_id != :id:',
            'bind' => array('name' => $area_name,
                            'id' => $area_id,)
        ));
    }
    public static function getNameByID ($area_id) {
        $result = OccArea::findFirstById($area_id);
        return $result ? $result->getAreaName() : '';
    }
    public function getNameByIDAndLang ($area_id,$lang = 'en') {
        $result = false;
        if ($lang && $lang != $this->globalVariable->defaultLanguage) {
            $sql = "SELECT a.*, al.* FROM \Offshore\Models\OccArea a
                INNER JOIN \Offshore\Models\OccAreaLang al
                ON al.area_id = a.area_id
                WHERE al.area_lang_code = '$lang'
                AND a.area_id = $area_id
                ";
            $lists = $this->modelsManager->executeQuery($sql)->getFirst();
            if($lists && sizeof($lists)>0){
                $result = \Phalcon\Mvc\Model::cloneResult(
                    new OccArea(),
                    [
                        "area_name" => $lists->al->getAreaName()
                    ]
                );
            }
        }else{
            $result = OccArea::findFirst(array(
                'area_id = :id:',
                'bind' => array('id' => $area_id)
            ));
        }
        return $result ? $result->getAreaName() : '';
    }
    public function getAreaById($area_id,$lang='en'){
        $result = false;
        if ($lang && $lang != $this->globalVariable->defaultLanguage) {
            $sql = "SELECT a.*, al.* FROM \Offshore\Models\OccArea a
                INNER JOIN \Offshore\Models\OccAreaLang al
                ON al.area_id = a.area_id
                WHERE al.area_lang_code = '$lang'
                AND a.area_active = 'Y'
                AND a.area_id = $area_id
                ";
            $lists = $this->modelsManager->executeQuery($sql)->getFirst();
            if($lists && sizeof($lists)>0){
                $result = \Phalcon\Mvc\Model::cloneResult(
                    new OccArea(),
                    [
                        "area_id" => $lists->a->getAreaId(),
                        "area_name" => $lists->al->getAreaName()
                    ]
                );
            }
        }else{
            $result = OccArea::findFirstById($area_id);
        }
        return $result;
    }
    public static function getCombobox($id)
    {
        $area = OccArea::find();
        $output = '';
        foreach ($area as $value)
        {
            $selected = '';
            if($value->getAreaId() == $id)
            {
                $selected = 'selected';
            }
            $output.= "<option ".$selected." value='".$value->getAreaId()."'>".$value->getAreaName()."</option>";

        }
        return $output;
    }
    public function getAllArea($lang='en')
    {
        $result = array();
        if ($lang && $lang != $this->globalVariable->defaultLanguage) {
            $sql = "SELECT a.*, al.* FROM \Offshore\Models\OccArea a 
                INNER JOIN \Offshore\Models\OccAreaLang al ON al.area_id = a.area_id AND al.area_lang_code = :LANG: 
                WHERE a.area_active = 'Y' 
                ORDER BY a.area_order ASC 
                ";
            $lists = $this->modelsManager->executeQuery($sql,array("LANG" => $lang));
            if($lists && sizeof($lists)>0){
                foreach ($lists as $item){
                    $result[] = \Phalcon\Mvc\Model::cloneResult(
                        new OccArea(),
                        [
                            "area_id" => $item->a->getAreaId(),
                            "area_name" => $item->al->getAreaName(),
                            "area_lat" => $item->a->getAreaLat(),
                            "area_lng" => $item->a->getAreaLng(),
                            "area_active" => $item->a->getAreaActive(),
                            "area_order" => $item->a->getAreaOrder()
                        ]
                    );
                }
            }
        }else{
            $list = OccArea::query()
                ->where("area_active = 'Y'")
                ->orderBy("area_order ASC")
                ->execute();
            if(sizeof($list)>0) $result = $list;
        }
        return $result;
    }
    public function getComboboxByOffice ($lang='en', $id){

        $area = $this->getAllArea($lang);
        $output = '';
        foreach ($area as $value)
        {
            $selected = '';
            if($value->getAreaId() == $id)
            {
                $selected = 'selected';
            }
            $output.= "<option ".$selected." value='".$value->getAreaId()."' data-lat = '".$value->getAreaLat()."' data-lng = '".$value->getAreaLng()."'>".$value->getAreaName()."</option>";

        }
        return $output;
    }
    public function getDefault()
    {
        return OccArea::findFirst(array(
                               'area_active = "Y"',
                               'order' => 'area_order ASC'));
    }
    public function getByCountryCode ($country_code) {
        $sql = 'SELECT a.* 
            FROM Offshore\Models\OccArea AS a
            INNER JOIN Offshore\Models\OccCountry AS c ON a.area_id = c.country_area_id AND c.country_active = "Y" 
            WHERE a.area_active="Y" AND c.country_code =:CODE:';
        return $this->modelsManager->executeQuery($sql,array('CODE' => $country_code))->getFirst();
    }
}
