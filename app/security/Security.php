<?php

class Security extends Phalcon\Mvc\User\Plugin
{

    /**
     * @var Phalcon\Acl\Adapter\Memory
     */
    protected $_acl;
    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    public function getAcl()
    {
       // if (!$this->_acl) {

            $acl = new Phalcon\Acl\Adapter\Memory();

            $acl->setDefaultAction(Phalcon\Acl::DENY);

            //Register roles
            $roles = array(
                'users' => new Phalcon\Acl\Role('Users'),
                'guests' => new Phalcon\Acl\Role('Guests')
            );
            foreach($roles as $role){
                $acl->addRole($role);
            }

            //Private area resources
            $privateResources = array(
                'news' => array('show', 'update', 'add', 'delete', 'save'),
                'language' => array('index', 'create', 'edit', 'delete'),
                'area' => array('index', 'create', 'edit', 'delete'),
            );
            foreach($privateResources as $resource => $actions){
                $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
            }

            //Private area resources
            $publicResources = array(
                'index' => array('index'),
                'login' => array('index', 'registerAccount', 'validateUser', 'goodbye')
            );
            foreach($publicResources as $resource => $actions){
                $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
            }

            //Grant access to public areas to both users and guests
            foreach($roles as $role){
                foreach($publicResources as $resource => $actions){
                    $acl->allow($role->getName(), $resource, '*');
                }
            }

            //Grant acess to private area to role Users
            foreach($privateResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow('Users', $resource, $action);
                }
            }

            $this->_acl = $acl;
      //  }
        return $this->_acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeDispatch(Phalcon\Events\Event $event, Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $auth = $this->session->get('human_Auth');

        if (!$auth){
            $role = 'Guests';
        } else {
            $role = 'Users';
        }
        echo $role;
//        print_r($this->session->get('human_Auth')); die;

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();


        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);
        if($action == 'goodbye')
        {
//            echo '<script> console.log("run good bye... ") </script>';
            echo "sss".$role.'---'.$controller.'---'.$action;
            die;
        }

//        var_dump(Phalcon\Acl::ALLOW); die;
        if ($allowed != Phalcon\Acl::ALLOW) {
//            echo "true != 1 ??"; die;
            $this->flashSession->error("You don't have access to this module");
            $this->response->redirect('/logout');
            return false;
        }

    }

}