<?php

// $router = $di->getRouter();
use Phalcon\Mvc\Router;

$router = new Router(false);
$router->setUriSource(
    Router::URI_SOURCE_SERVER_REQUEST_URI
);

$router->add(
    '/logout',
    [
        'controller' => 'login',
        'action'     => 'goodbye',
    ]
);

$router->add(
    '/login',
    [
        'controller' => 'login',
        'action'     => 'show',
    ]
);

$router->add(
    '/login/validate',
    [
        'controller' => 'login',
        'action'     => 'validateUser',
    ]
);

$router->add(
    '/account/register',
    [
        'controller' => 'login',
        'action'     => 'registerAccount',
    ]
);

$router->add(
    '/news/list',
    [
        'controller' => 'news',
        'action'     => 'show',
    ]
);

$router->add(
    '/news/add',
    [
        'controller' => 'news',
        'action'     => 'add',
    ]
);
$router->add(
    '/news/save',
    [
        'controller' => 'news',
        'action'     => 'save',
    ]
);

$router->add(
    '/news/update/([0-9]+)',
    [
        'controller' => 'news',
        'action'     => 'update',
        'params'     => 1,
    ]
);
$router->add(
    '/news/delete/([0-9]+)',
    [
        'controller' => 'news',
        'action'     => 'delete',
        'params'     => 1,
    ]
);


$router->add(
    '/test-the-router',
    [
        'controller' => 'testing',
        'action'     => 'index',
    ]
);
$router->add(
    '/test-the-sum/:controller/:action/:params', // http://test.theweb/test-the-sum/testing/sum/1/2
    [
        'controller' => 1,
        'action'     => 2,
        'params'     => 3,
    ]
);

$router->add(
    '/test-the-date/:params', // http://test.theweb/test-the-sum/testing/sum/1/2
    [
        'controller' => 'testing',
        'action'     => 'getDate',
        'params'     => 1,
    ]
);

$router->notFound(
    [
        'controller' => 'login',
        'action'     => 'notfound',
    ]
);

$router->add('/dashboard/create-area', array(
    "controller" => "area",
    "action"     => "create"
));
$router->add('/dashboard/edit-area', array(
    "controller" => "area",
    "action"	=> "edit"
));
$router->add('/dashboard/delete-area', array(
    "controller" => "area",
    "action"	=> "delete"
));

$router->add('/dashboard/list-language', array(

    "controller" => 'language',
    "action"	=> "index"
));
$router->add('/dashboard/create-language', array(

    "controller" => 'language',
    "action"	=> "create"
));
$router->add('/dashboard/delete-language', array(

    "controller" => 'language',
    "action"	=> "delete"
));
$router->add('/dashboard/edit-language', array(

    "controller" => 'language',
    "action"	=> "edit"
));

return $router;


